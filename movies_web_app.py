import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    # db = 'experiment'
    # username = 'root'
    # password = 'Dummy123'
    # hostname = 'localhost'
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE my_table(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year INT UNSIGNED NOT NULL, title TEXT NOT NULL, director TEXT NOT NULL, actor TEXT NOT NULL, release_date TEXT NOT NULL, rating FLOAT NOT NULL, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()
    # cur.execute("INSERT INTO "+ db+ " (greeting) values ('Hello, World!')")
    cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    cur.execute("SELECT id FROM my_table")
    entries = [dict(id=row[0]) for row in cur.fetchall()]
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_to_db():
    print("Received request.")
    year = request.form['year']
    title = request.form['title'].lower()
    director = request.form['director'].lower()
    actor = request.form['actor'].lower()
    release_date = request.form['release_date'].lower()
    rating = request.form['rating']
    print(request.form['year'])
    print(request.form['title'])
    print(request.form['director'])
    print(request.form['actor'])
    print(request.form['release_date'])
    print(request.form['rating'])


    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)


    try:
        cur = cnx.cursor()
        response = []
        cur.execute("SELECT Year, Title, Director, Actor, release_date, Rating FROM my_table WHERE Year='" + year + "' AND Title='" + title + "'")

        if not cur.fetchall():
            cur.execute("INSERT INTO my_table (Year, Title, Director, Actor, release_date, Rating) VALUES ('" + year + "', '" + title+ "', '" + director+ "', '" + actor+ "', '" + release_date+ "', '" + rating +"')")
            response = ["movie successfully inserted"]

        else:
            cur.execute("UPDATE my_table SET Director='" + director +"', Actor='" +actor+ "', Release_date='" +release_date + "', Rating='" +rating + "' WHERE Year='" + year + "' AND Title='" + title + "'")
            response = ["movie successfully updated"]

        cnx.commit()
        return hello(response)
    except Exception as e:
        response = ["Movie could not be updated/inserted with reason: " + str(e)]
        return hello(response)  

@app.route('/update_movie', methods=['POST'])
def update_to_db():
    print("Received request.")
    year = request.form['year']
    title = request.form['title'].lower()
    director = request.form['director'].lower()
    actor = request.form['actor'].lower()
    release_date = request.form['release_date'].lower()
    rating = request.form['rating']
    print(request.form['year'])
    print(request.form['title'])
    print(request.form['director'])
    print(request.form['actor'])
    print(request.form['release_date'])
    print(request.form['rating'])


    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)


    try:
        cur = cnx.cursor()
        response = []
        cur.execute("SELECT Year, Title, Director, Actor, release_date, Rating FROM my_table WHERE Year='" + year + "' AND Title='" + title + "'")

        if not cur.fetchall():
            cur.execute("INSERT INTO my_table (Year, Title, Director, Actor, release_date, Rating) VALUES ('" + year + "', '" + title+ "', '" + director+ "', '" + actor+ "', '" + release_date+ "', '" + rating +"')")
            response = ["movie successfully inserted"]

        else:
            cur.execute("UPDATE my_table SET Director='" + director +"', Actor='" +actor+ "', Release_date='" +release_date + "', Rating='" +rating + "' WHERE Year='" + year + "' AND Title='" + title + "'")
            response = ["movie successfully updated"]

        cnx.commit()
        return hello(response)
    except Exception as e:
        response = ["Movie could not be updated/inserted with reason: " + str(e)]
        return hello(response)  


@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print("Received request.")
    title = request.form['delete_title']
    print(request.form['delete_title'])


    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("SELECT Year, Title, Director, Actor, Release_date, Rating From my_table WHERE Title='"+ title +"'")
        results = cur.fetchall()
        if results:
            cur.execute("DELETE FROM my_table WHERE Title='" + title + "'")
            cnx.commit()
            response = ['Success: Deleted movie(s) with title as ' + title]
            return hello(response)
        else:
            response = ["Movie with title " + title + " does not exist"]
            return hello(response)
    except Exception as e:
        response = ["Movie " + title + " could not be deleted, exception is: " + str(e)]
        return hello(response)

@app.route('/search_movie', methods=['POST'])
def search_movie():
    print("Received request.")
    actor = request.form['search_actor'].lower()
    print(request.form['search_actor'])


    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("SELECT Year, Title, Director, Actor, Release_date, Rating From my_table WHERE Actor='"+ actor +"'")
        # cnx.commit()
        response = []
        results = cur.fetchall()
        if results:
            for row in results:
                response.append(str(row[1]) + ' ' + str(row[0]) + ' ' + str(row[3]))
            return hello(response)
        else:
            response = ["No movies found for actor " + actor]
            return hello(response)

    except Exception as e:
        response = ["Exception found! exception is: " + str(e)]
        return hello(response)

    

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    print("Received request.")


    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("SELECT Year, Title, Director, Actor, Release_date, Rating From my_table WHERE Rating = (SELECT MAX(Rating) from my_table)")
        # cnx.commit()
        response = []
        results = cur.fetchall()
        if results:
            for row in results:
                response.append(str(row[1]) + ' ' + str(row[0]) + ' ' + str(row[3]) + ' ' + str(row[2]) + ' ' + str(row[5]))
            return hello(response)
        else:
            response = ["no movies"]
    except Exception as e:
        response = ["Exception found! exception is: " + str(e)]
        return hello(response)

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    print("Received request.")
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("SELECT Year, Title, Director, Actor, Release_date, Rating From my_table WHERE Rating = (SELECT MIN(Rating) from my_table)")
        # cnx.commit()
        response = []
        results = cur.fetchall()
        if results:
            for row in results:
                response.append(str(row[1]) + ' ' + str(row[0]) + ' ' + str(row[3]) + ' ' + str(row[2]) + ' ' + str(row[5]))
            return hello(response)
        else:
            response = ["no movies"]
            return hello(response)
    except Exception as e:
        response = ["Exception found! exception is: " + str(e)]
        return hello(response)

@app.route("/")
def hello(output=[""]):
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    entries = query_data()
    print("Entries: %s" % entries)
    return render_template('index.html', entries=len(entries), output=output)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
